import { useState } from "react";
import React from 'react'
// import {Modal, Form, Button, FormGroup, Label, Input, } from 'reactstrap';
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    Label,
    Input,
    Table,
  } from "reactstrap";

  

function EditModal () {

  // const toggle = () => setModal(!modal);
  const [name, setName] = useState("");
  const [gender, setGender] = useState("");
  const [email, setEmail] = useState("");
  const [dept, setDept] = useState("");
  const [task, setTask] = useState([]);
  const [city, setCity] = useState("");
  const [number, setNumber] = useState("");
  const [error, setError] = useState(false);
  const [modal, setModal] = useState(false);

  const getTask = (e) => {
    const { value, checked } = e.target;
    if (checked) {
      setTask([...task, value]);
    } else {
      setTask(task.filter((e) => e !== value));
    }
  };


  return (
    <div>
<Modal >
          <ModalHeader>New Profile</ModalHeader>
          <ModalBody>
            <div className="Form">
              <Form>
                <FormGroup className="formG">
                  <Label className="formLabel">Name</Label>
                  <Input
                    className="Input"
                    value={name}
                    onChange={(event) => setName(event.target.value)}
                    type="text"
                    name="Name"
                    id="name"
                    placeholder="Enter Name"
                  />
                  {/* {error ? (
                    <label className="error"> Please Enter Name </label>
                  ) : (
                    ""
                  )} */}
                </FormGroup>

                <Label className="formLabel">Gender</Label>
                <FormGroup check>
                  <Input
                    type="radio"
                    value="Male"
                    name="Gender"
                    onClick={(event) => setGender(event.target.value)}
                  />
                  Male
                </FormGroup>

                <FormGroup check>
                  <Input
                    type="radio"
                    name="Gender"
                    value="Female"
                    onClick={(event) => setGender(event.target.value)}
                  />
                  Female
                </FormGroup>
                <FormGroup className="formG">
                  <Label className="formLabel">Email</Label>
                  <Input
                    className="Input"
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                    type="email"
                    name="email"
                    id="email"
                    placeholder="Enter email"
                  />
                  {/* {error ? (
                    <label className="error"> Please Enter Email</label>
                  ) : (
                    ""
                  )} */}
                </FormGroup>

                <FormGroup>
                  <Label> Department </Label>
                  <select
                    name="dept"
                    defaultValue="Department"
                    type="select"
                    value={dept}
                    onChange={(event) => setDept(event.target.value)}
                  >
                    <option>Department</option>
                    <option>Frontend</option>
                    <option>Backend</option>
                    <option>Human Resource</option>
                    <option>Operations</option>
                  </select>
                </FormGroup>

                <label> Tasks Completed:-</label>
                <FormGroup check>
                  <Input
                    type="checkbox"
                    name="Task1"
                    value="TASK1"
                    onClick={(event) => getTask(event)}
                  />
                  <Label check>TASK 1</Label>
                </FormGroup>

                <FormGroup check>
                  <Input
                    type="checkbox"
                    name="Task 2"
                    value=" TASK2"
                    onChange={(event) => getTask(event)}
                  />
                  <Label check>TASK 2</Label>
                </FormGroup>

                <FormGroup check>
                  <Input
                    type="checkbox"
                    name="Task3"
                    value=" TASK3"
                    onClick={(event) => getTask(event)}
                  />
                  <Label check>TASK 3</Label>
                </FormGroup>

                <FormGroup className="formG">
                  <Label className="formLabel">City</Label>
                  <Input
                    className="Input"
                    value={city}
                    onChange={(event) => setCity(event.target.value)}
                    type="text"
                    name="city"
                    id="city"
                    placeholder="Enter City"
                  />
                  {/* {error ? (
                    <Label className="error">"Please Enter City"</Label>
                  ) : (
                    ""
                  )} */}
                </FormGroup>

                <FormGroup className="formG">
                  <Label className="formLabel">Phone Number</Label>
                  <Input
                    className="Input"
                    value={number}
                    onChange={(event) => setNumber(event.target.value)}
                    type="Text"
                    name="number"
                    id="number"
                    placeholder="Enter Number"
                  />
                  {/* {error ? (
                    <Label className="error">"Please Enter Number"</Label>
                  ) : (
                    ""
                  )} */}
                </FormGroup>
              </Form>
            </div>
          </ModalBody>
          <ModalFooter></ModalFooter>
        </Modal>

    </div>
  )
}

export default EditModal;