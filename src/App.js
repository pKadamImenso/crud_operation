import "./App.css";
import EditModal from "./components/EditModal";
import axios from "axios";
import React, { useEffect, useState } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Input,
  Table,
} from "reactstrap";
// const baseURL = "http://localhost:7000/posts";

function App() {
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  const [name, setName] = useState("");
  const [gender, setGender] = useState("");
  const [email, setEmail] = useState("");
  const [dept, setDept] = useState("");
  const [task, setTask] = useState([]);
  const [city, setCity] = useState("");
  const [number, setNumber] = useState("");
  const [error, setError] = useState(false);
  const [mapData, setmapData] = useState([]);
  const [getting, setGetting] = useState(false);
  const [modalName, setModalName] = useState("");
  const [modalGender, setModalGender] = useState("");
  const [modalEmail, setModalEmail] = useState("");
  const [modalCity, setModalCity] = useState("");
  const [modalNumber, setModalNumber] = useState("");
  const [modalDept, setModalDept] = useState("");
  const [modalTask, setModalTask] = useState([]);
  const [modalData, setModalData] = useState([]);

  const Editer= async(id) => {

    let received =  await axios.get(`http://localhost:7000/posts?id=${id}`)
    setGetting(true);
    setModalName(received.data[0].name);
    // setModalGender(received.data[0].gender);
    setModalEmail(received.data[0].email);
    setModalCity(received.data[0].city);
    setModalNumber(received.data[0].number);  
    
  }


  const Update= (id)=>{
    setModalData([modalName,modalGender, modalEmail, modalDept, modalCity, modalTask, modalNumber]);
    // console.log("modaldata",modalData)
    const res=  axios.put(`http://localhost:7000/posts/${id}`, modalData)
   .then((response)=>{
    console.log(response)
   })
  }

  const deleteItemFromArray = async (id) => {
    // console.log("ifdddd", typeof id);~
    try {
      const response = await axios.delete(`http://localhost:7000/posts/` + id);
      // console.log(response);
    } catch (error) {
      console.error("Error deleting item:", error);
    }
  };

  const handleClick = () => {
    if (name === "") {
      setError(true);
    }
    if (email === "") {
      setError(true);
    }
    if (city === "") {
      setError(true);
    }
    if (number === "") {
      setError(true);
    } else {
      setName("");
      setEmail("");
      setCity("");
      setNumber("");

      postData({ name, gender, email, dept, task, city, number });
    }
  };

  const postData = (data) => {
    // console.log(22333, data);
    axios.post("http://localhost:7000/posts", data).then((response) => {
      getData();
    });
  };
  useEffect(() => {
    getData();
  }, []);
  const getData = () => {
    axios.get("http://localhost:7000/posts").then((getting) => {
      // console.log(getting, 5000);
      setmapData(getting.data);
    });
  };

  const getTask = (e) => {
    const { value, checked } = e.target;
    if (checked) {
      setTask([...task, value]);
    } else {
      setTask(task.filter((e) => e !== value));
    }
  };
  // console.log(5566, mapData);


  return (
    <div className="App">
   <Modal isOpen={getting}>
        <ModalHeader toggle={()=>setGetting(false)}>Edit</ModalHeader>
        <ModalBody>
        <Form>
                <FormGroup className="formG">
                  <Label className="formLabel">Name</Label>
                  <Input
                    className="Input" value={modalName} type="text"
                    onChange={(event) => setModalName(event.target.value)}
                  />
                  </FormGroup>

                  <Label className="formLabel">Gender</Label>
                <FormGroup check>
                  <Input
                    type="radio"
                    value={modalGender}
                    name="Male"
                    onClick={(event) => setModalGender(event.target.name)}
                  />
                  Male
                </FormGroup>

                <FormGroup check>
                  <Input
                    type="radio"
                    value={modalGender}
                    name="Female"
                    onClick={(event) => setModalGender(event.target.name)}
                  />
                  Female
                </FormGroup>

                  <FormGroup className="formG">
                  <Label className="formLabel">Email</Label>
                  <Input
                    className="Input"
                    value={modalEmail}
                    onChange={(event) => setModalEmail(event.target.value)}
                    type="text"
                   
                  />
                  </FormGroup>

                  <FormGroup>
              <Label> Department </Label>
              <select
                name="dept"
                defaultValue="Department"
                type="select"
                value={modalDept}
                onChange={(event) => setModalDept(event.target.value)}
              >
                <option>Department</option>
                <option>Frontend</option>
                <option>Backend</option>
                <option>Human Resource</option>
                <option>Operations</option>
              </select>
            </FormGroup>

                  <FormGroup className="formG">
                  <Label className="formLabel">City</Label>
                  <Input
                    className="Input"
                    value={modalCity}
                    onChange={(event) => setModalCity(event.target.value)}
                    type="text"
                  />
                  </FormGroup>
                  <label> Tasks Completed:-</label>
            <FormGroup check>
              <Input
                type="checkbox"
                name="Task1"
                value={modalTask}
                onChange={(event) => setModalTask(event)}
              />
              <Label check>TASK 1</Label>
            </FormGroup>

            <FormGroup check>
              <Input
                type="checkbox"
                name="Task2"
                value={modalTask}
                onChange={(event) => setModalTask(event)}
              />
              <Label check>TASK 3</Label>
            </FormGroup>

            <FormGroup check>
              <Input
                type="checkbox"
                name="Task3"
                value={modalTask}
                onChange={(event) => setModalTask(event)}
              />
              <Label check>TASK 3</Label>
            </FormGroup>

                  <FormGroup className="formG">
                  <Label className="formLabel">Number</Label>
                  <Input
                    className="Input" value={modalNumber} type="text"
                    onChange={(event) => setModalNumber(event.target.value)}
                  />
                  </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={(e)=>Update(e.id)}>
          Submit
          </Button>{' '}
          <Button color="secondary" onClick={()=>setGetting(false)}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
      <div className="heading">
        <h1>Candidates Information</h1>
      </div>
      <div className="tableDiv">
        <Table striped bordered className=" table">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Gender</th>
              <th>Email</th>
              <th>Department</th>
              <th>Task</th>
              <th>City</th>
              <th>Phone</th>
              <th>Delete</th>
              <th>Edit</th>
            </tr>
          </thead>
          <tbody>
            {mapData.map((element, index) => (
              <tr>
                <td>{element.index}</td>
                <td>{element.name}</td>
                <td>{element.gender}</td>
                <td>{element.email}</td>
                <td>{element.dept}</td>
                <td>{element.task}</td>
                <td>{element.city}</td>
                <td>{element.number}</td>
                <td>
                  {" "}
                  <Button
                    color="danger"
                    size="sm"
                    onClick={() => deleteItemFromArray(element.id)}
                  >
                    {" "}
                    Delete
                  </Button>
                </td>
                <td>
                  {" "}
                  <Button
                   color="warning" 
                   size="sm"
                   onClick={() => Editer(element.id) }
                   >
                    {" "}
                    Edit
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
      <div className="addNewBtn">
        <Button size="sm" color="primary" onClick={toggle}>
          Add New Profile
        </Button>
        <Modal isOpen={modal} toggle={toggle} >
          <ModalHeader toggle={toggle}>New Profile</ModalHeader>
          <ModalBody>
            <div className="Form">
              <Form>
                <FormGroup className="formG">
                  <Label className="formLabel">Name</Label>
                  <Input
                    className="Input"
                    value={name}
                    onChange={(event) => setName(event.target.value)}
                    type="text"
                    name="Name"
                    id="name"
                    placeholder="Enter Name"
                  />
                  {error ? (
                    <label className="error"> Please Enter Name </label>
                  ) : (
                    ""
                  )}
                </FormGroup>

                <Label className="formLabel">Gender</Label>
                <FormGroup check>
                  <Input
                    type="radio"
                    value="Male"
                    name="Gender"
                    onClick={(event) => setGender(event.target.value)}
                  />
                  Male
                </FormGroup>

                <FormGroup check>
                  <Input
                    type="radio"
                    name="Gender"
                    value="Female"
                    onClick={(event) => setGender(event.target.value)}
                  />
                  Female
                </FormGroup>
                <FormGroup className="formG">
                  <Label className="formLabel">Email</Label>
                  <Input
                    className="Input"
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                    type="email"
                    name="email"
                    id="email"
                    placeholder="Enter email"
                  />
                  {error ? (
                    <label className="error"> Please Enter Email</label>
                  ) : (
                    ""
                  )}
                </FormGroup>

                <FormGroup>
                  <Label> Department </Label>
                  <select
                    name="dept"
                    defaultValue="Department"
                    type="select"
                    value={dept}
                    onChange={(event) => setDept(event.target.value)}
                  >
                    <option>Department</option>
                    <option>Frontend</option>
                    <option>Backend</option>
                    <option>Human Resource</option>
                    <option>Operations</option>
                  </select>
                </FormGroup>

                <label> Tasks Completed:-</label>
                <FormGroup check>
                  <Input
                    type="checkbox"
                    name="Task1"
                    value="TASK1"
                    onClick={(event) => getTask(event)}
                  />
                  <Label check>TASK 1</Label>
                </FormGroup>

                <FormGroup check>
                  <Input
                    type="checkbox"
                    name="Task 2"
                    value=" TASK2"
                    onClick={(event) => getTask(event)}
                  />
                  <Label check>TASK 2</Label>
                </FormGroup>

                <FormGroup check>
                  <Input
                    type="checkbox"
                    name="Task3"
                    value=" TASK3"
                    onClick={(event) => getTask(event)}
                  />
                  <Label check>TASK 3</Label>
                </FormGroup>

                <FormGroup className="formG">
                  <Label className="formLabel">City</Label>
                  <Input
                    className="Input"
                    value={city}
                    onChange={(event) => setCity(event.target.value)}
                    type="text"
                    name="city"
                    id="city"
                    placeholder="Enter City"
                  />
                  {error ? (
                    <Label className="error">"Please Enter City"</Label>
                  ) : (
                    ""
                  )}
                </FormGroup>

                <FormGroup className="formG">
                  <Label className="formLabel">Phone Number</Label>
                  <Input
                    className="Input"
                    value={number}
                    onChange={(event) => setNumber(event.target.value)}
                    type="Text"
                    name="number"
                    id="number"
                    placeholder="Enter Number"
                  />
                  {error ? (
                    <Label className="error">"Please Enter Number"</Label>
                  ) : (
                    ""
                  )}
                </FormGroup>

                <Button onClick={handleClick}>Submit</Button>
              </Form>
            </div>
          </ModalBody>
          <ModalFooter></ModalFooter>
        </Modal>
      </div>
    </div>
  );
}

export default App;
